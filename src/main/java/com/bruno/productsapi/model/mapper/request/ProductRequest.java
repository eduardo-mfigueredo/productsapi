package com.bruno.productsapi.model.mapper.request;

import com.bruno.productsapi.model.entity.Categories;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "Product Request")
public class ProductRequest {

    @NotBlank(message = "Product Name must not be blank.") // This annotation requires the field to be not null and
    // have it's trimmed length greater than zero
    @Size(min = 5, max = 50, message = "Product Name must be between 5 and 50 characters.")
    @ApiModelProperty(position = 0, value = "The name of the Product", example = "Action Figure Kakashi")
    private String productName;
    @ApiModelProperty(position = 1, value = "The category for the Product", example = "action_figure")
    private Categories productCategory;

    @NotNull(message = "Product Price must not be blank.")
    @PositiveOrZero(message = "Product Price must not be negative.")
    @ApiModelProperty(position = 2, value = "The Product Price, in currency", example = "129.90")
    private Double productPrice;

    @NotNull(message = "Product weight must not be blank.")
    @PositiveOrZero(message = "Product weight must not be negative.")
    @ApiModelProperty(position = 3, value = "The Product Weight, in kilograms", example = "2.8")
    private Double productWeight;

    @NotNull(message = "Product height must not be blank.")
    @PositiveOrZero(message = "Product height must not be negative.")
    @ApiModelProperty(position = 4, value = "The Height of the Product, in centimeters", example = "18")
    private Integer productHeight;

    @NotNull(message = "Product width must not be blank.")
    @PositiveOrZero(message = "Product width must not be negative.")
    @ApiModelProperty(position = 4, value = "The Width of the Product, in centimeters", example = "8")
    private Integer productWidth;

    @NotNull(message = "Product length must not be blank.")
    @PositiveOrZero(message = "Product length must not be negative.")
    @ApiModelProperty(position = 4, value = "The Length of the Product, in centimeters", example = "5")
    private Integer productLength;


}
