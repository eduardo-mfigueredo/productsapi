package com.bruno.productsapi.model.mapper.request;

import com.bruno.productsapi.model.entity.ProductEntity;
import lombok.experimental.UtilityClass;

import java.time.LocalDateTime;

@UtilityClass // Marks the class as Static Final and can't have constructors
public class ProductRequestMapper {

    // This method converts the Product Request object to an Entity
    public static ProductEntity requestToEntity(ProductRequest productRequest) {

        return ProductEntity.builder()
                .productName(productRequest.getProductName())
                .productCategory(productRequest.getProductCategory())
                .productPrice(productRequest.getProductPrice())
                .productWeight(productRequest.getProductWeight())
                .productHeight(productRequest.getProductHeight())
                .productWidth(productRequest.getProductWidth())
                .productLength(productRequest.getProductLength())
                .updatedAt(LocalDateTime.now())
                .build();


    }

}
