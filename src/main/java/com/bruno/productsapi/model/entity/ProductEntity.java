package com.bruno.productsapi.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data // Lombok's annotation for creating Getters, Setters, Equals, HashCode and To String
@Builder // Lombok's annotation for building objects easily
@AllArgsConstructor // Constructor with all attributes using Lombok
@NoArgsConstructor // Empty Constructor using Lombok
@Document(collection = "Products")
// This annotation marks a class as being a domain object that we want to persist to the database
public class ProductEntity {

    @Id // This annotation marks this field as the ID for the Database
    private String id;
    private String productName;
    private Categories productCategory;
    private Double productPrice;
    private Integer inventory;

    private Double productWeight;
    private Integer productHeight;
    private Integer productWidth;
    private Integer productLength;

    private LocalDateTime updatedAt;

}
