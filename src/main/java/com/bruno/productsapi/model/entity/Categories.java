package com.bruno.productsapi.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Categories {

    @JsonProperty("canecas") // This annotation makes the keyword passed refer to this Enum Constant
    CANECAS,
    @JsonProperty("decoracao")
    DECORACAO,
    @JsonProperty("vestuario")
    VESTUARIO,
    @JsonProperty("action_figure")
    ACTION_FIGURE,
    @JsonProperty("funko_pop")
    FUNKO_POP,
    @JsonProperty("lego")
    LEGO

}
