package com.bruno.productsapi.controller;

import com.bruno.productsapi.configurations.annotations.*;
import com.bruno.productsapi.exceptions.entity_not_found.EntityNotFoundException;
import com.bruno.productsapi.integration.shipping.model.entity.ShippingEntity;
import com.bruno.productsapi.model.mapper.request.ProductRequest;
import com.bruno.productsapi.model.mapper.response.ProductResponse;
import com.bruno.productsapi.service.ProductServiceFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


// The Controller Class contains the REST Endpoints for the Application
// It is the initial point for all requests
@RestController // This annotation marks this class as a Rest Controller
@RequestMapping("v1/products") // This annotation marks this class as an endpoint for the API
@RequiredArgsConstructor // Lombok constructor for the dependency injections
@Api(value = "Products API") // Swagger Annotation for the API title
//TODO Study Global Config
@CrossOrigin(origins = "*") // Allows all IP addresses to access this API
public class ProductController {

    // Dependency injection
    private final ProductServiceFacade productServiceFacade;

    // Route to calculate shipping passing a Product ID and a destination CEP
    @SwaggerShippingInit
    @GetMapping("/v1/shipping")
    //Todo Get with Body
    public List<ShippingEntity> calculateShipping(
            @RequestParam(value = "id") @ApiParam(name = "id", value = "Product ID", example = "62f0f6b3973cb841c7817ab1") String id,
            @RequestParam(value = "cep") @ApiParam(name = "cep", value = "Destination CEP code", example = "95625000") String cep) {

        return productServiceFacade.calculateShipping(id, cep);
    }

    // Route to Get One or more Products from the Database
    @SwaggerGetAllInit
    @GetMapping("/v1") // Indicates this endpoint as a GET Http Method
    public List<ProductResponse> getProducts(
            @RequestParam(value = "ids", required = false, defaultValue = "")
            @ApiParam(name = "ids", value = "List of Product ID's", example = "62f0f6b3973cb841c7817ab1") List<String> ids,
            @RequestParam(value = "query", required = false, defaultValue = "")
            @ApiParam(name = "query", value = """
                    Define a Query:
                    productnameasc - Returns all Products in Alphabetical Order.
                    productnamedesc - Returns all Products in Reverse Alphabetical Order.
                    priceasc - Returns All Products in Ascending Price Order.
                    pricedesc - Returns All Producs in Descending Price Order.""", example = "productnameasc") String query) {
        return productServiceFacade.getProducts(ids, query);
    }

    @GetMapping("/v1/findbyname")
    public List<ProductResponse> getProductsByName(@RequestParam String name) {
        return productServiceFacade.getProductsByQuery(name);
    }

    // Route to get a Pageable Object for querying and display control.
    @SwaggerGetPageableInit
    @GetMapping("/v1/pageable") // Indicates this endpoint as a GET Http Method
    // Needs Swagger to @ApiIgnore the Pageable, otherwise it displays the wrong parameters
    public Page<ProductResponse> getProductsPageable(@ApiIgnore() Pageable pageable) {
        return productServiceFacade.getProductsPageable(pageable);
    }

    // Route to Add a Product to the Database
    @SwaggerPostInit
    @PostMapping("/v1") // Indicates this endpoint as a POST Http Method
    @ResponseStatus(HttpStatus.CREATED) // Indicates the Http Status Code for the response
    public ProductResponse addProduct(@RequestBody @Valid @ApiParam(name = "Product Body", value = "a JSON representing the product, " +
            "with name, category, price and dimensions.") ProductRequest product) {
        return productServiceFacade.addProduct(product);
    }

    // Route to Update a Product in the Database
    @SwaggerPutInit
    @PutMapping("/v1/{id}") // Indicates this endpoint as a PUT Http Method
    @ResponseStatus(HttpStatus.CREATED) // Indicates the Http Status Code for the response
    public ProductResponse updateProduct(
            @RequestBody @Valid @ApiParam(name = "Product Body", value = "a JSON representing the product, " +
                    "with name, category, price and dimensions.") ProductRequest productRequest,
            @PathVariable @ApiParam(name = "id", value = "Product ID to be updated.", example = "62f0f6b3973cb841c7817ab1")
            String id) {
        return productServiceFacade.updateProduct(productRequest, id);
    }

    // Route to Delete One or More Products from the Database
    @SwaggerDeleteInit
    @DeleteMapping("/v1") // Indicates this endpoint as a DELETE Http Method
    @ResponseStatus(HttpStatus.NO_CONTENT) // Indicates the Http Status Code for the response
    public void deleteProducts(
            @RequestParam(required = false, defaultValue = "") @ApiParam(name = "id", value = "One or more product ID's " +
                    "to be deleted.", example = "62f0f6b3973cb841c7817ab1") List<String> ids,
            @RequestParam(required = false, defaultValue = "false") @ApiParam(name = "deleteAll", value = "Flag to confirm" +
                    " deletion of entire database.", example = "false") Boolean deleteAll) {
        productServiceFacade.deleteProducts(ids, deleteAll);
    }


    //----------------------------------------------------------------------------------------------------------------

    // For Cookie Creation we use the HttpServletResponse to create a Cookie object and send it in the response.
    @GetMapping("/v1/cookie") // Indicates this endpoint as a GET Http Method
    @ResponseStatus(HttpStatus.CREATED) // Indicates the Http Status Code for the response
    @ApiIgnore
    public String createCookie(HttpServletResponse response) {

        // Creates a new Cookie passing the Local Date Time formatted
        // to be stored in the user's browser.
        DateTimeFormatter dtf = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
        Cookie cookie = new Cookie("Cookie", dtf.format(LocalDateTime.now()));
        response.addCookie(cookie);
        return "Cookie set!";
    }

    // For Cookie reading we can use the HttpServletRequest
    @GetMapping("/v1/when")
    @ResponseStatus(HttpStatus.OK)
    @ApiIgnore
    public String whenLastAccess(HttpServletRequest request) {
        // Check if there's cookies in the request
        Cookie[] cookies = Optional.ofNullable(request.getCookies()).orElseThrow(() -> new EntityNotFoundException("No cookies found"));
        // Check if there's a cookie for Last Time Accessed
        LocalDateTime lastAccess = LocalDateTime.parse(Arrays.stream(cookies)
                .filter(cookie -> {
                    var name = cookie.getName();
                    return "Cookie".equals(name);
                })
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Cookie not found")).getValue());
        LocalDateTime now = LocalDateTime.now();

        long days = ChronoUnit.DAYS.between(lastAccess, now);

        return "It's been " + days + " days since your last access.";
    }


}
