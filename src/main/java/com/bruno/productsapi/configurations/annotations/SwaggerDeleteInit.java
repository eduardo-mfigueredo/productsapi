package com.bruno.productsapi.configurations.annotations;

import com.bruno.productsapi.exceptions.ExceptionDetails;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@ApiOperation(value = "Deletes one or more Products", notes = "Deletes a product or products from the Database. " +
        "If the flag deleteAll is passed, then clears the entire Database.")
@ApiResponses(value = {
        @ApiResponse(code = 204, message = "Success."),
        @ApiResponse(code = 400, message = "Bad request.",
                response = ExceptionDetails.class)
})
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SwaggerDeleteInit {

}
