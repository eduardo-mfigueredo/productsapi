package com.bruno.productsapi.repository;

import com.bruno.productsapi.model.entity.ProductEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ProductRepository extends MongoRepository<ProductEntity, String>{


    List<ProductEntity> findAllByOrderByProductNameAsc();
    List<ProductEntity> findAllByOrderByProductNameDesc();
    List<ProductEntity> findAllByOrderByProductPriceAsc();
    List<ProductEntity> findAllByOrderByProductPriceDesc();
}

