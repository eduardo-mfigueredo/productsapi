package com.bruno.productsapi.exceptions;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.time.Instant;

@Data
@SuperBuilder
@ApiModel(value = "Exceptions")
public class ExceptionDetails {

    @ApiModelProperty(value = "The name for the exception", example = "Entity Not Found")
    protected String name;
    @ApiModelProperty(position = 1, value = "The timestamp when the exception occurred", example = "2022-08-25T14:05:53.843345168Z")
    protected Instant timestamp; // Let debug be a timestamp for now
    @ApiModelProperty(position = 2, value = "The message describing the exception", example = "ID not found = null")
    protected String message;
    @ApiModelProperty(position = 3, value = "A link for documentation concerning this exception", example = "https://www.baeldung.com/hibernate-entitynotfoundexception")
    protected String link;

}
