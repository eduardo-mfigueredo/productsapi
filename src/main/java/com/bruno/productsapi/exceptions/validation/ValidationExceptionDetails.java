package com.bruno.productsapi.exceptions.validation;

import com.bruno.productsapi.exceptions.ExceptionDetails;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class ValidationExceptionDetails extends ExceptionDetails {


}
