package com.bruno.productsapi.exceptions.validation;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ValidationException extends RuntimeException{
        // Custom exception for Entity Not Found
        // Uses the constructor for the RuntimeException class


    }

