package com.bruno.productsapi.exceptions.entity_not_found;

import com.bruno.productsapi.exceptions.ExceptionDetails;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

// Class for instantiating a custom Exception object
@SuperBuilder
@Getter
public class EntityNotFoundExceptionDetails extends ExceptionDetails {

}
