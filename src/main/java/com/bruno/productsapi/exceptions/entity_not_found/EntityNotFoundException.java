package com.bruno.productsapi.exceptions.entity_not_found;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends  RuntimeException{

    // Custom exception for Entity Not Found
    // Uses the constructor for the RuntimeException class
    public EntityNotFoundException(String msg) {
        super(msg);
    }

}
