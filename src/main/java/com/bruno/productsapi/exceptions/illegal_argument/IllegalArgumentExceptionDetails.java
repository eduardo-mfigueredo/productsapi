package com.bruno.productsapi.exceptions.illegal_argument;

import com.bruno.productsapi.exceptions.ExceptionDetails;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

// Class for instantiating a custom Exception object
@SuperBuilder
@Getter
public class IllegalArgumentExceptionDetails extends ExceptionDetails {
}
