package com.bruno.productsapi.exceptions;

import com.bruno.productsapi.exceptions.entity_not_found.EntityNotFoundException;
import com.bruno.productsapi.exceptions.entity_not_found.EntityNotFoundExceptionDetails;
import com.bruno.productsapi.exceptions.illegal_argument.IllegalArgumentExceptionDetails;
import com.bruno.productsapi.exceptions.validation.ValidationExceptionDetails;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.UnexpectedTypeException;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

// This class handles all the exceptions that can be triggered by the API automatically
// as long as there's a method declared for that specific Exception

@ControllerAdvice // This annotation marks this class as the global exception handler
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    //TODO Response Entity

    @ExceptionHandler(EntityNotFoundException.class) // Annotation marking this method for resolving the type of
    // exception passed as an argument
    public ResponseEntity<EntityNotFoundExceptionDetails> handleEntityNotFound(EntityNotFoundException e) {
        return new ResponseEntity<>(
                EntityNotFoundExceptionDetails.builder()
                        .name("Entity Not Found")
                        .timestamp(Instant.now())
                        .message(e.getMessage())
                        .link("https://www.baeldung.com/hibernate-entitynotfoundexception")
                        .build(), HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<IllegalArgumentExceptionDetails> handleIllegalArgumentException(IllegalArgumentException e) {
        return  new ResponseEntity<>(
            IllegalArgumentExceptionDetails.builder()
                    .name("Illegal Argument")
                    .timestamp(Instant.now())
                    .message("Your query didn't match any results.")
                    .link("#")
                    .build(), HttpStatus.BAD_REQUEST
        );
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException e, HttpHeaders headers, HttpStatus status, WebRequest request) {

        List<FieldError> fieldErrors = e.getBindingResult().getFieldErrors();

        String fieldsMessage = fieldErrors.stream()
                .map(FieldError::getDefaultMessage)
                .collect(Collectors.joining(" "));

        return new ResponseEntity<>(
                ValidationExceptionDetails.builder()
                        .name("Invalid input")
                        .timestamp(Instant.now())
                        .message(fieldsMessage)
                        .link("#")
                        .build(), HttpStatus.BAD_REQUEST
        );
    }


    protected ResponseEntity<Object> UnexpectedTypeException(UnexpectedTypeException e) {


        return new ResponseEntity<>(
                ValidationExceptionDetails.builder()
                        .name("Invalid input")
                        .timestamp(Instant.now())
                        .message(e.getMessage())
                        .link("#")
                        .build(), HttpStatus.BAD_REQUEST
        );
    }

    @NotNull
    @Override
    protected ResponseEntity<Object> handleExceptionInternal(
            Exception e, @Nullable Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {

        return new ResponseEntity<>(ExceptionDetails.builder()
                .name("Something is wrong")
                .timestamp(Instant.now())
                .message(e.getMessage())
                .link("#")
                .build(), status);
    }


}
