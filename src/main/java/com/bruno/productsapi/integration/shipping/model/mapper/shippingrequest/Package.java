package com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Package {

    Integer height;
    Integer width;
    Integer length;
    Double weight;
}
