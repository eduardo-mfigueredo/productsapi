package com.bruno.productsapi.integration.shipping.resttemplate;

import com.bruno.productsapi.exceptions.entity_not_found.EntityNotFoundException;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingresponse.ShippingResponse;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest.ShippingRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ShippingClient {

    private final RestTemplate restTemplateMelhorEnvio;

    public List<ShippingResponse> fetchShippingData(ShippingRequest shippingRequest) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<ShippingRequest> entity = new HttpEntity<>(shippingRequest, headers);

        return List.of(Optional.ofNullable(restTemplateMelhorEnvio.postForObject("/api/v2/me/shipment/calculate", entity, ShippingResponse[].class)).orElseThrow(() -> new EntityNotFoundException("No response")));


    }
}
