package com.bruno.productsapi.integration.shipping.resttemplate;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;


// Configuration for the Rest Template to be used in integrating the External API

@Configuration
public class ShippingClientConfiguration {

    @Bean
    public RestTemplate restTemplateMelhorEnvio() {

        //Todo Global config and Error Handler
        return new RestTemplateBuilder()

                .rootUri("https://melhorenvio.com.br/") // Sets the base URL to be used
                // in all requests that this that this particular Rest Template executes.
                .defaultHeader("Authorization", // Auth data to be sent in all requests
                        "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImY3ZGM2YTVhNTQzZjYxMjk4YTAzNjgxNzVkODlmMzkzYTA2M2RjODc4NzRlZDJkMjQxMzVlYmFlMDhmZWE1NGMzYWQ4MTRlMDYzNjNiZTU4In0.eyJhdWQiOiIxIiwianRpIjoiZjdkYzZhNWE1NDNmNjEyOThhMDM2ODE3NWQ4OWYzOTNhMDYzZGM4Nzg3NGVkMmQyNDEzNWViYWUwOGZlYTU0YzNhZDgxNGUwNjM2M2JlNTgiLCJpYXQiOjE2NjAxNDIxNzEsIm5iZiI6MTY2MDE0MjE3MSwiZXhwIjoxNjkxNjc4MTcxLCJzdWIiOiJhMWExZjEyOC0wODY3LTRjYjYtOWQ1OC1iZmZiYmIyM2MyOWIiLCJzY29wZXMiOlsiY2FydC1yZWFkIiwiY2FydC13cml0ZSIsImNvbXBhbmllcy1yZWFkIiwiY29tcGFuaWVzLXdyaXRlIiwiY291cG9ucy1yZWFkIiwiY291cG9ucy13cml0ZSIsIm5vdGlmaWNhdGlvbnMtcmVhZCIsIm9yZGVycy1yZWFkIiwicHJvZHVjdHMtcmVhZCIsInByb2R1Y3RzLWRlc3Ryb3kiLCJwcm9kdWN0cy13cml0ZSIsInB1cmNoYXNlcy1yZWFkIiwic2hpcHBpbmctY2FsY3VsYXRlIiwic2hpcHBpbmctY2FuY2VsIiwic2hpcHBpbmctY2hlY2tvdXQiLCJzaGlwcGluZy1jb21wYW5pZXMiLCJzaGlwcGluZy1nZW5lcmF0ZSIsInNoaXBwaW5nLXByZXZpZXciLCJzaGlwcGluZy1wcmludCIsInNoaXBwaW5nLXNoYXJlIiwic2hpcHBpbmctdHJhY2tpbmciLCJlY29tbWVyY2Utc2hpcHBpbmciLCJ0cmFuc2FjdGlvbnMtcmVhZCIsInVzZXJzLXJlYWQiLCJ1c2Vycy13cml0ZSIsIndlYmhvb2tzLXJlYWQiLCJ3ZWJob29rcy13cml0ZSIsInRkZWFsZXItd2ViaG9vayJdfQ.f-K2yKeWNV1Anq_q-LkGU4bl6sCVOYGOybojxMLePgCdFZNMr5NYVkb8MjxKBg6DVC4rKA-zZBtsv8qF1eLTqqcaTmVdD_BzCh7D6FoS3zjzrZh7c7aqa6jmUSWbXGnlTN1W9Jx0pDxtLJPTgWzVzmik74WkMUfjOtAupEND0b6HuB_pT9OBnbhriz9WMoAzLDhl7ZmZLxqMW4WYxIA3vsZlRn-wOdVuF07w0MPyP2G9DOQPjBUKS0Dh_BzIAVbm9n9AcooRPsfNkmCs9VHvKMiVeoIsfBQ2ncxUiICtPElskJqxzdqF83rqwgAocWZqCjwBY6YJ0u5kgNi3yVVI4To29Anv8ez-RqyEu1RpJSRssdGyKqSJeqp6MyvtTOQBfaWADxn9lVJs26OgPZiyay9tC0zIPZwjyDjqDSsqetQDmQ3TYdomau3WcugPv-p7qlXmXgVlBw2xctL8F6ViTtmXauel03eAi56anTeH9E01OsAljUmo22GAVPJjF4faNFqSFb_YL3idL0Jr4wajGFEZIVc2GvqYIDsZZCyLrpIi7vTBek-G8Pm2v_c0baW5YPgwXVhMg-SF0CV4O-5h5BRMewiadoyntPFxpbJJ9arE3n97DetX-YWxUbL4BgAJSk4shRZeZZffiVVqFr1sFg3jJWaLpCYBDuRIOROas-8")
                .defaultHeader("Content-Type", String.valueOf(MediaType.APPLICATION_JSON))

                .build();
    }
}
