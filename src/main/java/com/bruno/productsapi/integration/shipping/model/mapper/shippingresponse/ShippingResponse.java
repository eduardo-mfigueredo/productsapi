package com.bruno.productsapi.integration.shipping.model.mapper.shippingresponse;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ShippingResponse {

    private int id;
    private String name;
    private String price;
    @JsonProperty("delivery_time")
    private int deliveryTime;
    private Company company;
    private String error;


}
