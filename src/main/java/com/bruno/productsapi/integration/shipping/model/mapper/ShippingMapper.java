package com.bruno.productsapi.integration.shipping.model.mapper;

import com.bruno.productsapi.integration.shipping.model.mapper.shippingresponse.ShippingResponse;
import com.bruno.productsapi.integration.shipping.model.entity.ShippingEntity;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest.From;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest.Package;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest.ShippingRequest;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest.To;
import com.bruno.productsapi.model.entity.ProductEntity;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ShippingMapper {

    // This method receives a Product Entity and a String CEP, and returns a Shipping Request
    public static ShippingRequest productEntityToShippingRequest(ProductEntity productEntity, String cep) {

        return ShippingRequest.builder()
                .from(From.builder().postalCode("91030320").build()) // CEP Origin (Macro Office)
                .to(To.builder().postalCode(cep).build()) // CEP Destination
                .packageInfo(Package.builder()
                        .height(productEntity.getProductHeight())
                        .width(productEntity.getProductWidth())
                        .length(productEntity.getProductLength())
                        .weight(productEntity.getProductWeight())
                        .build())
                .build();
    }

    // This method receives a Shipping Response and returns a Shipping Entity
    public static ShippingEntity shippingResponseToShippingEntity(ShippingResponse shippingResponse) {

        return ShippingEntity.builder()
                .company(shippingResponse.getCompany().getName())
                .shippingPrice(Double.valueOf(shippingResponse.getPrice()))
                .deliveryName(shippingResponse.getName())
                .deliveryTime(shippingResponse.getDeliveryTime())
                .build();
    }

}
