package com.bruno.productsapi.integration.shipping;

import com.bruno.productsapi.exceptions.entity_not_found.EntityNotFoundException;
import com.bruno.productsapi.integration.shipping.model.entity.ShippingEntity;
import com.bruno.productsapi.integration.shipping.model.mapper.ShippingMapper;
import com.bruno.productsapi.integration.shipping.resttemplate.ShippingClient;
import com.bruno.productsapi.model.entity.ProductEntity;
import com.bruno.productsapi.repository.ProductRepository;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest.ShippingRequest;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingresponse.ShippingResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ShippingService {

    private final ProductRepository productRepository;
    private final ShippingClient shippingClient;

    // Logic for calculating Shipping value, delivery time and company
    // By passing a product ID, get the Product Entity from the DB
    // Map the Entity into a Shipping Request object to send it to the Rest Template Client
    public List<ShippingEntity> calculateShipping(String id, String cep) {
        //Todo hide Entity
        ProductEntity productEntity = productRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("ID not found = " + id));
        ShippingRequest shippingRequest = ShippingMapper.productEntityToShippingRequest(productEntity, cep);
        List<ShippingResponse> shippingResponses = shippingClient.fetchShippingData(shippingRequest);

        // If nothing is returned, then an Exception has occurred
        if (shippingResponses.isEmpty()) {
            throw new EntityNotFoundException("No results");
        } else {
            // Else, filter the results and map it to a Shipping Entity object
            return shippingResponses.stream()
                    .filter(shippingResponse -> shippingResponse.getError() == null)
                    .map(ShippingMapper::shippingResponseToShippingEntity)
                    .toList();
        }
    }
}
