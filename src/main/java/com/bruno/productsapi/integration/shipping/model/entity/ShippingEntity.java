package com.bruno.productsapi.integration.shipping.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Shipping Response")
public class ShippingEntity {

    @ApiModelProperty(position = 0, value = "The name for the Delivery Company", example = "Correios")
    String company;
    @ApiModelProperty(position = 1, value = "The type of Delivery within the company", example = "PAC")
    String deliveryName;
    @ApiModelProperty(position = 2, value = "The Delivery Price, in currency", example = "23.85")
    Double shippingPrice;
    @ApiModelProperty(position = 3, value = "The Delivery time, in days", example = "4")
    Integer deliveryTime;

}
