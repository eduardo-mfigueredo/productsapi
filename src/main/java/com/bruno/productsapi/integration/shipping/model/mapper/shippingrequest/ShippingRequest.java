package com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

// Class used for sending requests to the Integration ENDPOINT - produces a JSON Object to be used in the body
// of the request

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ShippingRequest {

    From from;
    To to;
    @JsonProperty("package")
    Package packageInfo;


}


