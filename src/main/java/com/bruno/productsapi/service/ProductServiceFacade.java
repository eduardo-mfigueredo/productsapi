package com.bruno.productsapi.service;

import com.bruno.productsapi.integration.shipping.model.entity.ShippingEntity;
import com.bruno.productsapi.model.mapper.response.ProductResponse;
import com.bruno.productsapi.integration.shipping.ShippingService;
import com.bruno.productsapi.model.mapper.request.ProductRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
//Todo Isolated Service Using facade
public class ProductServiceFacade {

    private final ProductService productService;

    private final ShippingService shippingService;

    public List<ProductResponse> getProducts(List<String> ids, String query) {
       return productService.getProductsLogic(ids, query);
    }

    public List<ProductResponse> getProductsByQuery(String name) {
       return productService.getProductsByQuery(name);
    }

    public Page<ProductResponse> getProductsPageable(Pageable pageable) {
        return productService.getProductsPageable(pageable);
    }

    public ProductResponse addProduct(ProductRequest productRequest) {
        return productService.addProduct(productRequest);
    }

    public ProductResponse updateProduct(ProductRequest productRequest, String id) {
        return productService.updateProduct(productRequest, id);
    }

    public void deleteProducts(List<String> ids, Boolean deleteAll) {
        productService.deleteProducts(ids, deleteAll);
    }

    public List<ShippingEntity> calculateShipping(String id, String cep) {
        return shippingService.calculateShipping(id, cep);
    }


}
