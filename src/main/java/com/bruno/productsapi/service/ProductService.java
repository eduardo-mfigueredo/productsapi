package com.bruno.productsapi.service;

import com.bruno.productsapi.exceptions.entity_not_found.EntityNotFoundException;
import com.bruno.productsapi.model.entity.ProductEntity;
import com.bruno.productsapi.model.mapper.request.ProductRequest;
import com.bruno.productsapi.model.mapper.request.ProductRequestMapper;
import com.bruno.productsapi.model.mapper.response.ProductResponse;
import com.bruno.productsapi.model.mapper.response.ProductResponseMapper;
import com.bruno.productsapi.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

// The Service Class implements the logic behind every request made to the Controller
// It stands between the Controller and the Repository

@Service // This annotation marks this class as a Service
@RequiredArgsConstructor
public class ProductService {

    // Dependency injection
    private final ProductRepository productRepository;
    private final MongoTemplate mongoTemplate;

    // Method to Retrieve All Products that Contains the Query in the Name Attribute
    // TODO: Refactor using custom Mongo Template
    public List<ProductResponse> getProductsByQuery(String name) {
        Query query = new Query();

        query.addCriteria(Criteria.where("productName").regex(StringUtils.capitalize(name)));
        List<ProductEntity> products = mongoTemplate.find(query, ProductEntity.class);
        return products.stream().map(ProductResponseMapper::entityToResponse).toList();
    }

    // Method that decides which of the Get Products Methods to use
    // TODO: Refactor using custom Mongo Template
    public List<ProductResponse> getProductsLogic(List<String> ids, String query) {
        if (query.isEmpty()) {
            if (ids.isEmpty()) {
                return this.getAllProducts();
            } return this.getProductsById(ids);

        }
        String formattedQuery = query.toLowerCase();
        return switch (formattedQuery) {
            case "productnameasc" -> this.getAllProductsOrderByNameAscending();
            case "productnamedesc" -> this.getAllProductsOrderByNameDescending();
            case "priceasc" -> this.getAllProductsOrderByPriceAscending();
            case "pricedesc" -> this.getAllProductsOrderByPriceDescending();
            default -> throw new IllegalArgumentException();
        };
    }

    // Method to Retrieve All Products from the Database
    public List<ProductResponse> getAllProducts() {
        return productRepository.findAll().stream().map(ProductResponseMapper::entityToResponse).toList();
    }

    // Method to Retrieve Products from the Database  by Passing a Product ID
    // TODO: Refactor!
    public List<ProductResponse> getProductsById(List<String> ids) {
        List<ProductResponse> productResponses = new ArrayList<>();
        for (String id : ids) {
            productResponses.add(ProductResponseMapper.entityToResponse(productRepository.findById(id)
                    .orElseThrow(() -> new EntityNotFoundException("ID not found - " + id))));
        }
        return productResponses;
    }

    // Method to Retrieve All Products from the Database Ordered By Name Ascending
    public List<ProductResponse> getAllProductsOrderByNameAscending() {
        return productRepository.findAllByOrderByProductNameAsc().stream().map(ProductResponseMapper::entityToResponse).toList();
    }

    // Method to Retrieve All Products from the Database Ordered By Name Descending
    public List<ProductResponse> getAllProductsOrderByNameDescending() {
        return productRepository.findAllByOrderByProductNameDesc().stream().map(ProductResponseMapper::entityToResponse).toList();
    }

    // Method to Retrieve All Products from the Database Ordered By Price Ascending
    public List<ProductResponse> getAllProductsOrderByPriceAscending() {
        return productRepository.findAllByOrderByProductPriceAsc().stream().map(ProductResponseMapper::entityToResponse).toList();
    }

    // Method to Retrieve All Products from the Database Ordered By Name Descending
    public List<ProductResponse> getAllProductsOrderByPriceDescending() {
        return productRepository.findAllByOrderByProductPriceDesc().stream().map(ProductResponseMapper::entityToResponse).toList();
    }

    // Logic for GET request to generate a Pageable response
    public Page<ProductResponse> getProductsPageable(Pageable pageable) {

        Page<ProductEntity> page = productRepository.findAll(pageable);
        return page.map(ProductResponseMapper::entityToResponse);

    }

    // Logic for the POST request to add a product to the database
    public ProductResponse addProduct(ProductRequest product) {

        // Convert the Product Request object to an Entity to save in the Database
        ProductEntity productEntity = ProductRequestMapper.requestToEntity(product);

        // Save the Entity in the Database
        productRepository.save(productEntity);

        // Converts the Entity to a Product Response object for returning

        return ProductResponseMapper.entityToResponse(productEntity);
    }

    // Logic for the PUT request to update some or all product attributes in the Database
    public ProductResponse updateProduct(ProductRequest product, String id) {
        // Check to see if the ID is valid first
        ProductEntity productEntitySaved = productRepository.findById(id).orElseThrow(() -> // Mongo method to find and return an item by ID
                new EntityNotFoundException("ID not found = " + id));  // Custom exception for ID not found));

        ProductEntity productEntityToSave = ProductRequestMapper.requestToEntity(product); // ID is null at this point

        productEntityToSave.setId(productEntitySaved.getId()); // Get ID from DB and set it in new Product

        return ProductResponseMapper.entityToResponse(productRepository.save(productEntityToSave));
    }

    // Logic for the DELETE request to remove one or more products with given IDs from the Database.
    // If no ID is passed it deletes the entire Database
    public void deleteProducts(List<String> ids, Boolean deleteAll) {
        if (deleteAll) {
            productRepository.deleteAll();
        } else if (ids.isEmpty()) {
            throw new EntityNotFoundException("No ID was passed");
        } else {
            for (String id : ids) {
                // Check to see if the id exists, if it does, delete it. Otherwise, throw Exception.
                productRepository.deleteById(productRepository.findById(id).orElseThrow(() ->
                        new EntityNotFoundException("ID not found = " + id)).getId());
            }
        }
    }


}
