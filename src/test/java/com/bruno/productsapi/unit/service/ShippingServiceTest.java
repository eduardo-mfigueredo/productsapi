package com.bruno.productsapi.unit.service;

import com.bruno.productsapi.exceptions.entity_not_found.EntityNotFoundException;
import com.bruno.productsapi.integration.shipping.model.entity.ShippingEntity;
import com.bruno.productsapi.integration.shipping.resttemplate.ShippingClient;
import com.bruno.productsapi.repository.ProductRepository;
import com.bruno.productsapi.integration.shipping.ShippingService;
import com.bruno.productsapi.util.ShippingStubs;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static com.bruno.productsapi.util.ProductStubs.createProductEntityStub;

@SpringBootTest
@AutoConfigureMockMvc
@DisplayName("Tests for the Shipping Service Layer")
class ShippingServiceTest {

    @InjectMocks
    ShippingService shippingService;

    @Mock
    ShippingClient shippingClient;

    @Mock
    ProductRepository productRepository;

    @Test
    @DisplayName("Should Return Shipping Entity if ID and CEP passed are valid.")
    void calculateShipping_ReturnListOfShippingEntities_WhenIdAndCepAreValid() {
        // Given
        BDDMockito.when(shippingClient.fetchShippingData(ShippingStubs.createShippingRequest()))
                .thenReturn(List.of(ShippingStubs.createShippingResponse()));
        BDDMockito.when(productRepository.findById("1"))
                .thenReturn(Optional.ofNullable(createProductEntityStub()));
        String expectedCompany = ShippingStubs.createShippingEntity().getCompany();

        //When
        List<ShippingEntity> shippingEntities = shippingService.calculateShipping("1", "1");

        //Then
        Assertions.assertThat(shippingEntities)
                .isNotEmpty()
                .isNotNull();
        Assertions.assertThat(shippingEntities.get(0).getCompany()).isEqualTo(expectedCompany);
    }

    @Test
    @DisplayName("Should Throw Entity Not Found Exception if ID passed is invalid.")
    void calculateShipping_ThrowsException_WhenIdIsInvalid() {
        //Given
        BDDMockito.when(productRepository.findById(ArgumentMatchers.anyString()))
                .thenThrow(new EntityNotFoundException("ID not found."));

        //When
        try {
            shippingService.calculateShipping("1", "1");
        } catch (Exception e) {
            //Then
            Assertions.assertThat(e)
                    .isInstanceOf(EntityNotFoundException.class);
            Assertions.assertThat(e.getMessage())
                    .isEqualTo("ID not found.");
        }
    }

    @Test
    @DisplayName("Should Throw Entity Not Found Exception when Shipping Response List is Empty.")
    void calculateShipping_ThrowsException_WhenShippingResponseListIsEmpty() {
        //Given
        BDDMockito.when(productRepository.findById("1")).thenReturn(Optional.ofNullable(createProductEntityStub()));
        BDDMockito.when(shippingClient.fetchShippingData(ArgumentMatchers.any()))
                .thenReturn(List.of());

        //When
        try {
            shippingService.calculateShipping("1", "2");

        } catch (Exception e) {
            //Then
            Assertions.assertThat(e)
                    .isInstanceOf(EntityNotFoundException.class);
            Assertions.assertThat(e.getMessage())
                    .isEqualTo("No results");
        }
    }
}
