package com.bruno.productsapi.unit.service;

import com.bruno.productsapi.exceptions.entity_not_found.EntityNotFoundException;
import com.bruno.productsapi.exceptions.validation.ValidationException;
import com.bruno.productsapi.model.entity.ProductEntity;
import com.bruno.productsapi.model.mapper.request.ProductRequest;
import com.bruno.productsapi.model.mapper.response.ProductResponse;
import com.bruno.productsapi.repository.ProductRepository;
import com.bruno.productsapi.service.ProductService;
import com.bruno.productsapi.util.ProductStubs;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static com.bruno.productsapi.util.ProductStubs.*;


@SpringBootTest
@AutoConfigureMockMvc
@DisplayName("Tests for the Product Service Layer")
//TODO Mock Server
class ProductServiceTest {

    @InjectMocks
    private ProductService productService;

    @Mock
    private ProductRepository productRepository;

    @Test
    @DisplayName("Should return a list of Product Responses when Successful")
    void getAllProducts_ShouldReturnAListOfProductResponses_WhenSuccessful() {
        //Given
        BDDMockito.when(productRepository.findAll()).thenReturn(List.of(ProductStubs.createProductEntityStub()));
        String expectedName = createProductEntityStub().getProductName();

        //When
        List<ProductResponse> allProducts = productService.getAllProducts();

        //Then
        Assertions.assertThat(allProducts).hasSize(1);
        Assertions.assertThat(allProducts.get(0).getProductName()).isEqualTo(expectedName);
    }

    @Test
    @DisplayName("Should return a list of Product Responses when Successful")
    void getProductsById_ShouldReturnAListOfProductResponses_WhenSuccessful() {
        //Given
        BDDMockito.when(productRepository.findById("1")).thenReturn(Optional.ofNullable(createProductEntityStub()));
        String expectedName = createProductEntityStub().getProductName();

        //When
        List<ProductResponse> productsById = productService.getProductsById(List.of("1"));

        //Then
        Assertions.assertThat(productsById).hasSize(1);
        Assertions.assertThat(productsById.get(0).getProductName()).isEqualTo(expectedName);
    }

    @Test
    @DisplayName("Should throw Entity Not Found Exception when ID passed is invalid.")
    void getProductsById_ShouldThrowEntityNotFoundException_WhenIdPassedIsInvalid() {
        //When
        try {
            productService.getProductsById(List.of("Invalid ID"));
        } catch (Exception e) {
            //Then
            Assertions.assertThat(e).isInstanceOf(EntityNotFoundException.class);
            Assertions.assertThat(e.getMessage()).isEqualTo("ID not found - Invalid ID");
        }
    }

    @Test
    @DisplayName("Should return a list of Product Responses when successful")
    void getAllProductsOrderByNameAscending_ShouldReturnAListOfProductResponses_WhenSuccessful() {
        //Given
        BDDMockito.when(productRepository.findAllByOrderByProductNameAsc()).thenReturn(
                List.of(ProductStubs.createProductEntityStub()));
        String expectedName = createProductEntityStub().getProductName();

        //When
        List<ProductResponse> allProductsOrderByNameAscending = productService.getAllProductsOrderByNameAscending();

        //Then
        Assertions.assertThat(allProductsOrderByNameAscending).isNotEmpty();
        Assertions.assertThat(allProductsOrderByNameAscending.get(0).getProductName()).isEqualTo(expectedName);
    }

    @Test
    @DisplayName("Should return a list of Product Responses when successful")
    void getAllProductsOrderByNameDescending_ShouldReturnAListOfProductResponses_WhenSuccessful() {
        //Given
        BDDMockito.when(productRepository.findAllByOrderByProductNameDesc()).thenReturn(
                List.of(ProductStubs.createProductEntityStub()));
        String expectedName = createProductEntityStub().getProductName();

        //When
        List<ProductResponse> allProductsOrderByNameDescending = productService.getAllProductsOrderByNameDescending();

        //Then
        Assertions.assertThat(allProductsOrderByNameDescending).isNotEmpty();
        Assertions.assertThat(allProductsOrderByNameDescending.get(0).getProductName()).isEqualTo(expectedName);
    }

    @Test
    @DisplayName("Should return a list of Product Responses when successful")
    void getAllProductOrderByPriceAscending_ShouldReturnAListOfProductResponses_WhenSuccessful() {
        //Given
        BDDMockito.when(productRepository.findAllByOrderByProductPriceAsc()).thenReturn(
                List.of(ProductStubs.createProductEntityStub()));
        String expectedName = createProductEntityStub().getProductName();

        //When
        List<ProductResponse> allProductsOrderByPriceAscending = productService.getAllProductsOrderByPriceAscending();

        //Then
        Assertions.assertThat(allProductsOrderByPriceAscending).isNotEmpty();
        Assertions.assertThat(allProductsOrderByPriceAscending.get(0).getProductName()).isEqualTo(expectedName);
    }

    @Test
    @DisplayName("Should return a list of Product Responses when successful")
    void getAllProductsOrderByPriceDescending_ShouldReturnAListOfProductResponses_WhenSuccessful() {
        //Given
        BDDMockito.when(productRepository.findAllByOrderByProductPriceDesc()).thenReturn(
                List.of(ProductStubs.createProductEntityStub()));
        String expectedName = createProductEntityStub().getProductName();

        //When
        List<ProductResponse> allProductsOrderByPriceDescending = productService.getAllProductsOrderByPriceDescending();

        //Then
        Assertions.assertThat(allProductsOrderByPriceDescending).isNotEmpty();
        Assertions.assertThat(allProductsOrderByPriceDescending.get(0).getProductName()).isEqualTo(expectedName);
    }

    @Test
    @DisplayName("Add Products should return the saved product as a Product Response when successful")
    void addProducts_ShouldReturnProductResponse_WhenSuccessful() {
        //Given
        BDDMockito.when(productRepository.save(createProductEntityStub()))
                .thenReturn(createProductEntityStub());
        String expectedName = createProductRequestStub().getProductName();

        //When
        ProductResponse productResponse = productService.addProduct(createProductRequestStub());

        //Then
        Assertions.assertThat(productResponse)
                .isNotNull();

        Assertions.assertThat(productResponse.getProductName())
                .isEqualTo(expectedName);
    }

    @Test
    @DisplayName("Add Products throws Validation Exception when the Product Request fails the Validation")
    void addProducts_ThrowsValidationException_WhenInvalidProductRequestIsPassed() {
        //When
        try {
            productService.addProduct(createInvalidProductRequestStub());
        } catch (Exception e) {
            //Then
            Assertions.assertThat(e).isInstanceOf(ValidationException.class);
            Assertions.assertThat(e.getMessage()).isEqualTo("Product Price must not be negative.");
        }
    }

    @Test
    @DisplayName("Update Product should not throw exception when passed ID and Product Request are valid")
    void updateProduct_ShouldReturnProductResponse_WhenIdAndProductRequestAreValid() {
        //Given
        BDDMockito.when(productRepository.findById("1")).thenReturn(Optional.ofNullable(createProductEntityStub()));
        BDDMockito.when(productRepository.save(ArgumentMatchers.isA(ProductEntity.class)))
                .thenReturn(createProductEntityStub());
        ProductRequest productToUpdate = createProductRequestStub();

        //When
        ProductResponse productResponse = productService.updateProduct(productToUpdate, "1");

        //Then
        Assertions.assertThat(productResponse.getProductName()).isEqualTo(productToUpdate.getProductName());
    }

    @Test
    @DisplayName("Update Product should throw Entity Not Found Exception when ID is invalid")
    void updateProduct_ShouldThrowEntityNotFoundException_WhenIdIsInvalid() {
        //When
        try {
            productService.updateProduct(createProductRequestStub(), "invalid ID");
        } catch (Exception e) {
            //Then
            Assertions.assertThat(e).isInstanceOf(EntityNotFoundException.class);
            Assertions.assertThat(e.getMessage()).isEqualTo("ID not found = invalid ID");
        }
    }

    @Test
    @DisplayName("Update Product should throw Entity Not Found Exception when ID is invalid")
    void updateProduct_ShouldThrowEntityNotFoundException_WhenIdPassedIsInvalid() {
        //When
        try {
            productService.updateProduct(createProductRequestStub(), "invalid ID");
        } catch (Exception e) {
            //Then
            Assertions.assertThat(e).isInstanceOf(EntityNotFoundException.class);
            Assertions.assertThat(e.getMessage()).isEqualTo("ID not found = invalid ID");
        }
    }

    @Test
    @DisplayName("Delete Products should throw Entity Not Found Exception when no ID is passed")
    void deleteProducts_ShouldThrowEntityNotFoundException_WhenNoIdIsPassed() {
        //When
        try {
            productService.deleteProducts(List.of(), false);
        } catch (Exception e) {
            //Then
            Assertions.assertThat(e).isInstanceOf(EntityNotFoundException.class);
            Assertions.assertThat(e.getMessage()).isEqualTo("No ID was passed");
        }
    }

    @Test
    @DisplayName("Delete Products should throw Entity Not Found Exception when ID passed is Invalid")
    void deleteProducts_ShouldThrowEntityNotFoundException_WhenNoIdPassedIsInvalid() {
        //When
        try {
            productService.deleteProducts(List.of("invalid ID"), false);
        } catch (Exception e) {
            //Then
            Assertions.assertThat(e).isInstanceOf(EntityNotFoundException.class);
            Assertions.assertThat(e.getMessage()).isEqualTo("ID not found = invalid ID");
        }
    }

    @Test
    @DisplayName("Delete Products should not throw any exception when successful")
    void deleteProducts_ShouldNotThrowException_WhenSuccessful() {
        //Given
        BDDMockito.doNothing().when(productRepository).deleteById("1");
        BDDMockito.when(productRepository.findById("1")).thenReturn(Optional.ofNullable(createProductEntityStub()));

        //Then
        Assertions.assertThatCode(() -> productService.deleteProducts(List.of("1"), false))
                .doesNotThrowAnyException();
    }
}