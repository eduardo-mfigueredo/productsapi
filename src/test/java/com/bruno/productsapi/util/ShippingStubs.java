package com.bruno.productsapi.util;

import com.bruno.productsapi.integration.shipping.model.entity.ShippingEntity;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest.From;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest.Package;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest.ShippingRequest;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingrequest.To;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingresponse.Company;
import com.bruno.productsapi.integration.shipping.model.mapper.shippingresponse.ShippingResponse;

public class ShippingStubs {

    public static ShippingEntity createShippingEntity() {
        return ShippingEntity.builder()
                .shippingPrice(1.0)
                .deliveryTime(1)
                .company("Test Company")
                .deliveryName("Test Delivery Name")
                .build();
    }

    public static ShippingRequest createShippingRequest() {
        return ShippingRequest.builder()
                .from(From.builder().postalCode("91030320").build())
                .to(To.builder().postalCode("1").build())
                .packageInfo(Package.builder().width(1).length(1).height(1).weight(1.0).build())
                .build();
    }

    public static ShippingResponse createShippingResponse() {
        return ShippingResponse.builder()
                .id(1)
                .company(Company.builder().id(1).name("Test Company").picture("Test picture").build())
                .price("1")
                .name("Test Delivery Name")
                .deliveryTime(1)
                .build();
    }

    public static ShippingResponse createShippingResponseWithError() {
        return ShippingResponse.builder()
                .id(1)
                .company(Company.builder().id(1).name("Test Company").picture("Test picture").build())
                .price("1")
                .name("Test Delivery Name")
                .deliveryTime(1)
                .error("Test Error")
                .build();
    }
}
